package com.example.tarancics;

import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class EdebiHayati extends Activity implements OnClickListener {

	@SuppressWarnings("unused")
	private TextView txtEHB;
	private TextView txtEH;
	Button btnArti;
	Button btnEksi;
	String text;
	Integer boyut = 15;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.edebihayati);

		txtEHB = (TextView) findViewById(R.id.textBaslik_EdebiHayati);
		txtEH = (TextView) findViewById(R.id.text_EdebiHayati);
		txtEH.setMovementMethod(new ScrollingMovementMethod());

		btnArti = (Button) findViewById(R.id.btn_Buyut);
		btnArti.setOnClickListener(this);
		btnEksi = (Button) findViewById(R.id.btn_Kucult);
		btnEksi.setOnClickListener(this);

		AssetManager assetManager = getAssets();
		InputStream input;

		try {
			input = assetManager.open("cst_EdebiHayati.txt");

			int size = input.available();
			byte[] buffer = new byte[size];
			input.read(buffer);
			input.close();
			String text = new String(buffer);
			txtEH.setTextSize(boyut);
			txtEH.setText(text);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.btn_Buyut:
			boyut += 3;
			txtEH.setTextSize(boyut);
			break;

		case R.id.btn_Kucult:
			boyut -= 3;
			txtEH.setTextSize(boyut);
			break;

		default:
			// code..
			break;
		}
	}
}
