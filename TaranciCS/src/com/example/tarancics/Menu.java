package com.example.tarancics;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Menu extends Activity implements OnClickListener {

	private StartAppAd startAppAd = new StartAppAd(this);
	Intent intentActivity;
	private Button mnBiyografi;
	private Button mnEdebiHayati;
	private Button mnResimler;
	private Button mnsiirler;

	Context mContext = Menu.this;
	SharedPreferences appPreferences;
	boolean isAppInstalled = false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
        StartAppAd.init(this, "109072322", "209524550");
	    StartAppAd.showSlider(this);
	    
		setContentView(R.layout.menu); // Our only layout for this app is
										// main.xml

		mnBiyografi = (Button) findViewById(R.id.mnBiyografi);
		mnBiyografi.setOnClickListener(this);

		mnEdebiHayati = (Button) findViewById(R.id.mnEdebiHayati);
		mnEdebiHayati.setOnClickListener(this);

		mnResimler = (Button) findViewById(R.id.mnResimler);
		mnResimler.setOnClickListener(this);

		mnsiirler = (Button) findViewById(R.id.mnsiirler);
		mnsiirler.setOnClickListener(this);

		/**
		 * check if application is running first time, only then create shorcut
		 */
		appPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		isAppInstalled = appPreferences.getBoolean("isAppInstalled", false);
		if (isAppInstalled == false) {
			/**
			 * create short code
			 */
			Intent shortcutIntent = new Intent(getApplicationContext(),
					Menu.class);
			shortcutIntent.setAction(Intent.ACTION_MAIN);
			Intent intent = new Intent();
			intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
			intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, "Cahit S�tk� Taranc�");
			intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
					Intent.ShortcutIconResource.fromContext(
							getApplicationContext(), R.drawable.cst_app));
			intent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
			getApplicationContext().sendBroadcast(intent);
			/**
			 * Make preference true
			 */
			SharedPreferences.Editor editor = appPreferences.edit();
			editor.putBoolean("isAppInstalled", true);
			editor.commit();
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.mnBiyografi:
			intentActivity = new Intent(v.getContext(), Biyografi.class);
			startActivity(intentActivity);
			break;

		case R.id.mnEdebiHayati:
			intentActivity = new Intent(v.getContext(), EdebiHayati.class);
			startActivity(intentActivity);
			break;

		case R.id.mnsiirler:
			intentActivity = new Intent(v.getContext(),
					MainTaranciActivity.class);
			startActivity(intentActivity);
			break;

		case R.id.mnResimler:
			intentActivity = new Intent(v.getContext(),
					AndroidGridLayoutActivity.class);
			startActivity(intentActivity);
			break;

		default:
			// code..
			break;
		}
	}
}
