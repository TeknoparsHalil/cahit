package com.example.tarancics;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

public class MainTaranciActivity extends Activity {

	private ListView lvBaslik;
	private final int REQ_CODE_SPEECH_INPUT = 100;

	ArrayAdapter<String> adapter;
	ArrayAdapter<String> adapter2;

	ImageButton btnSpeak;
	EditText inputSearch;
	ArrayList<HashMap<String, String>> productList;
	DatabaseSiir db;
	String[][] siir;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_taranci);

		db = new DatabaseSiir(this);
		final List<dbSiir> siirListesi = db.getAllSiir();

		lvBaslik = (ListView) findViewById(R.id.list_view);
		inputSearch = (EditText) findViewById(R.id.inputSearch);
		btnSpeak = (ImageButton) findViewById(R.id.btnSpeak);

		File folder = new File(Environment.getExternalStorageDirectory()
				+ "/���RLER");
		boolean success = true;
		if (!folder.exists()) {
			success = folder.mkdir();
		}
		if (success) {
			// Do something on success
		} else {
			// Do something else on failure
		}

		String[] b = new String[siirListesi.size()];
		String[] s = new String[siirListesi.size()];
		siir = new String[siirListesi.size()][2];
		int i = 0;
		for (dbSiir dbSiir : siirListesi) {
			b[i] = dbSiir.getBaslik();
			s[i] = dbSiir.getSiir();
			siir[i][0] = b[i];
			siir[i][1] = s[i];
			i++;
		}

		adapter = new ArrayAdapter<String>(this, R.layout.list_item,
				R.id.product_name, b);
		lvBaslik.setAdapter(adapter);

		inputSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2,
					int arg3) {

				MainTaranciActivity.this.adapter.getFilter().filter(
						cs.toString());
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable arg0) {
				if (arg0.length() == 0) {
					lvBaslik.clearTextFilter();
					lvBaslik.setAdapter(adapter);
				}
			}
		});

		lvBaslik.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				Intent i = new Intent(getApplicationContext(), Detay.class);
				String bas = MainTaranciActivity.this.adapter.getItem(position);
				i.putExtra("baslik", bas);
				i.putExtra("siir", find(bas));
				// i.putExtra("getir", (String)
				// parent.getItemAtPosition(position));
				startActivity(i);
			}
		});

		// getActionBar().hide();

		btnSpeak.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				promptSpeechInput();
			}
		});
	}

	String find(String s) {
		for (int i = 0; i < siir.length; i++) {
			if (siir[i][0].equals(s)) {
				return siir[i][1];
			}
		}
		return "amk";

	}

	/**
	 * Showing google speech input dialog
	 * */
	private void promptSpeechInput() {
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
				RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
		intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
				getString(R.string.speech_prompt));
		try {
			startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
		} catch (ActivityNotFoundException a) {
			Toast.makeText(getApplicationContext(),
					getString(R.string.speech_not_supported),
					Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * Receiving speech input
	 * */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case REQ_CODE_SPEECH_INPUT: {
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> result = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
				// txtSpeechInput.setText(result.get(0));
				inputSearch.setText(result.get(0));
			}
			break;
		}

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_taranci, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
