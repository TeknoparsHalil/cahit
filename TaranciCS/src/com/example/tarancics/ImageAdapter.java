package com.example.tarancics;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter {
	private Context mContext;
	// Keep all Images in array
	public Integer[] mThumbIds = { R.drawable.cst_02, R.drawable.cst_03,
			R.drawable.cst_04, R.drawable.cst_05, R.drawable.cst_06,
			R.drawable.cst_07, R.drawable.cst_08, R.drawable.cst_09,
			R.drawable.cst_10, R.drawable.cst_11, R.drawable.cst_12,
			R.drawable.cst_13, R.drawable.cst_14, R.drawable.cst_15,
			R.drawable.cst_16, R.drawable.cst_17, R.drawable.cst_18,
			R.drawable.cst_19, R.drawable.cst_20, R.drawable.cst_21 };

	// Constructor
	public ImageAdapter(Context c) {
		mContext = c;
	}

	@Override
	public int getCount() {
		return mThumbIds.length;
	}

	@Override
	public Object getItem(int position) {
		return mThumbIds[position];
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView = new ImageView(mContext);
		imageView.setImageResource(mThumbIds[position]);
		return imageView;

	}

}
