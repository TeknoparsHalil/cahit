package com.example.tarancics;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseSiir extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "dbCST";
	private static final String TABLE_CONTACTS = "Table_CST";

	@SuppressWarnings("unused")
	private String siirList;

	public DatabaseSiir(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public List<dbSiir> getAllSiir() {
		List<dbSiir> siirList = new ArrayList<dbSiir>();
		String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				dbSiir siirler = new dbSiir();
				siirler.setID(Integer.parseInt(cursor.getString(0)));
				siirler.setBaslik(cursor.getString(1));
				siirler.setSiir(cursor.getString(2));
				siirList.add(siirler);
			} while (cursor.moveToNext());
		}
		return siirList;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		try {
			String CREATE_CONTACTS_TABLE = "CREATE TABLE Table_CST (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , baslik TEXT NOT NULL , siir TEXT NOT NULL )";

			db.execSQL(CREATE_CONTACTS_TABLE);
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Abbas','Abbas');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Ak�am Vakti','AksamVakti');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Anlamak','Anlamak');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Anne Ne Yapt�n','AnneNeYaptin');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('A�k Adam�','AskAdami');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('A�k �le','AskIle');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('A�k�m�z','Askimiz');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('A�k Masal�','AskMasali');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('A�k','Ask');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Atat�rk � D���n�rken','AtaturkuDusunurken');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Avu�lar�ma S��m�yor Y�ld�zlar','AvuclarimaSigmiyorYildizlar');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Bahar Yeli','BaharYeli');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Batan Gemi','BatanGemi');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Ben A�k Adam�y�m','BenAskAdamiyim');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Ben �lecek Adam De�ilim','BenOlecekAdamDegilim');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Bir Bayram Yeme�inde','BirBayramYemeginde');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Bir de Bakm���m ki �lm���m','BirdeBakmisimkiOlmusum');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Bir G�zel','BirGuzel');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Bir Kap� A��p Gitsem','BirKapiAcipGitsem');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Bir Lahzam','BirLahzam');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Bir Umut','BirUmut');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Biz Nerdeyiz Sevgilim','BizNerdeyizSevgilim');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Bug�n Cuma','BugunCuma');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Bug�n Hava G�zel','BugunHavaGuzel');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Can Yolda��','CanYoldasi');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('�ilingir Sofras�','CilingirSofrasi');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('�ocukluk','Cocukluk');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('��k�yorum','Cokuyorum');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Dalg�n �l�','DalginOlu');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Dar Kal�p','DarKalip');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('De�i�ik','Degisik');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Deniz','Deniz');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Desem Ki','Desemki');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('D��ten G�zel','DustenGuzel');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Eski Saadetinle','EskiSaadetinle');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Esmer G�zeli Yarim','EsmerGuzeliYarim');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Fani D�nya','FaniDunya');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Garip Ki�i','GaripKisi');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Gariplik','Gariplik');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Gece Bir Neticedir','GeceBirNeticedir');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Gece �ark�s�','GeceSarkisi');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Gel �ad�r Kur','GelCadirKur');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Gen�lik B�yledir ��te','GenclikBoyledirIste');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Gidiyorum','Gidiyorum');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('G�n Eksilmesin Penceremden','GunEksilmesinPenceremden');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('G�n Olur ki','GunOlurki');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('G�nd�z','Gunduz');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('G�ne�e Ait �ocuk','GuneseAitCocuk');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Hat�ralar','Hatiralar');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Hep Ya�ad���ma Dair','HepYasadigimaDair');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Hepimize Dair','HepimizeDair');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Her Gece mi Bu Uykusuzluk','HerGecemiUykusuzluk');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Her G�nk� �l�m','HerGunkuOlum');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('�ki Ses','IkiSes');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('�lk A�klar','IlkAsklar');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('�mkans�z Dostluk','ImkansizDostluk');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('�nsano�lu','Insanoglu');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Kar ve Ben','KarveBen');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Kara Sevda','KaraSevda');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Korktu�um �ey','KorktugumSey');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Korkun� G�zel','KorkuncGuzel');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Korkulu K�pr�','KorkuluKopru');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Kulak Ver ki','KulakVerki');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Madem ki Vakit Ak�am','MademkiVakitAksam');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Memleket �sterim','MemleketIsterim');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Memleket','Memleket');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Me�gale','Mesgale');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Misafir','Misafir');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Neden Sonra','NedenSonra');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Otuz Be� Ya� �iiri','OtuzBesYasSiiri');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('�l�mden Sonra','OlumdanSonra');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('�mr�mde S�kut','OmrumdeSukut');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Paydos','Paydos');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Peri�an Sofra','PerisanSofra');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Robenson','Robenson');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('R�yam�z','Ruyamiz');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Sabah Duas�','SabahDuasi');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Sanatkar�n �l�m�','SanatkarinOlumu');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Say�klayan A�a�','SayiklayanAgac');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Sen de Her�ey Gibi','SendeHerSeyGibi');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Sen Yoktun ki','SenYoktunki');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Serenad','Serenad');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Son Gece','SonGece');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('�a��rd�m Kald�m','SasirdimKaldim');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('�a�k�n D�nya','SaskinDunya');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('�ubat G�n�','SubatGunu');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Talihsiz','Talihsiz');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Tren','Tren');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Tutsam Ellerinden A�lars�n','TutsamEllerindenAglarsin');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Var','Var');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Ya�mur Ya�adursun','YagmurYagadursun');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Yalan D�nya','YalanDunya');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Yaln�zl�k Maceras�','YalnizlikMacerasi');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Yaln�zl�k','Yalnizlik');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Yan�lg�','Yanilgi');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Ya��m �lerledik�e','YasimIlerledikce');");
			db.execSQL("INSERT INTO Table_CST (baslik,siir) VALUES ('Y�rek','Yurek');");
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			db.close();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}
}
