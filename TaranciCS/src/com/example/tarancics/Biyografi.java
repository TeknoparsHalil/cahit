package com.example.tarancics;

import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Biyografi extends Activity implements OnClickListener {

	AssetManager assetManager;
	InputStream input;
	TextView tv;
	Button btnArti;
	Button btnEksi;
	String text;
	Integer boyut = 15;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.biyografi);

		tv = (TextView) findViewById(R.id.text_Biyografi);
		tv.setMovementMethod(new ScrollingMovementMethod());

		btnArti = (Button) findViewById(R.id.btn_Buyut);
		btnArti.setOnClickListener(this);
		btnEksi = (Button) findViewById(R.id.btn_Kucult);
		btnEksi.setOnClickListener(this);

		assetManager = getAssets();

		try {
			input = assetManager.open("cst_Biyografi.txt");

			int size = input.available();
			byte[] buffer = new byte[size];
			input.read(buffer);
			input.close();
			text = new String(buffer);
			tv.setTextSize(boyut);
			tv.setText(text);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.btn_Buyut:
			boyut += 3;
			tv.setTextSize(boyut);
			break;

		case R.id.btn_Kucult:
			boyut -= 3;
			tv.setTextSize(boyut);
			break;

		default:
			// code..
			break;
		}
	}
}
