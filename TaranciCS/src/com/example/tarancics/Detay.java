package com.example.tarancics;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Detay extends Activity {

	private TextView tvb;
	private WebView wvs;
	private String baslik, siir;
	private Intent iDetay;
	private AlertDialog.Builder builder;
	private MediaPlayer media;
	private MediaRecorder myAudioRecorder;
	private File folder;
	private String outputFile = null;
	private Button kayit, kaydet, oynat;
	private Button gonder;
	private int oynatKontrol = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detay);

		try {
			tvb = (TextView) findViewById(R.id.textBaslik);
			wvs = (WebView) findViewById(R.id.webview_compontent);
			kayit = (Button) findViewById(R.id.btn_Kayit);
			kaydet = (Button) findViewById(R.id.btn_Kaydet);
			oynat = (Button) findViewById(R.id.btn_Play);
			gonder = (Button) findViewById(R.id.btn_Gonder);

			iDetay = getIntent();
			baslik = iDetay.getStringExtra("baslik");
			siir = iDetay.getStringExtra("siir");

			klasorAc();
			dosyaAc();
			kaydediciAc();

			tvb.setText(baslik);
			wvs.loadUrl("file:///android_asset/" + siir + ".html");

		} catch (Exception e) {
			// TODO: handle exception
		}

		butonKontrol(true, true, false, true);
	}

	public void butonKontrol(Boolean btn1, Boolean btn2, Boolean btn3,
			Boolean btn4) {
		try {
			oynat.setEnabled(btn1);
			kayit.setEnabled(btn2);
			kaydet.setEnabled(btn3);
			gonder.setEnabled(btn4);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void dosya(View view) {

		try {
			File myfile = new File(outputFile);
			if (myfile.exists()) {
				Intent share = new Intent(Intent.ACTION_SEND);
				share.setType("audio/*");
				share.putExtra(Intent.EXTRA_STREAM, Uri.parse(outputFile));
				startActivity(Intent.createChooser(share, "Payla�"));
			} else {
				showMesaj("mp3 dosyas� bulunmad�!", 1000);

			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void klasorAc() {
		try {
			folder = new File(Environment.getExternalStorageDirectory()
					+ "/���RLER" + "/" + baslik);
			boolean success = true;
			if (!folder.exists()) {
				success = folder.mkdirs();
			}
			if (success) {
				// Do something on success
			} else {
				// Do something else on failure
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public String dosyaAc() {
		outputFile = Environment.getExternalStorageDirectory()
				.getAbsolutePath()
				+ "/���RLER"
				+ "/"
				+ baslik
				+ "/"
				+ baslik
				+ ".mp4";
		;
		return outputFile;
	}

	public void kaydediciAc() {
		try {
			myAudioRecorder = new MediaRecorder();
			myAudioRecorder.setOutputFile(outputFile);
			myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
			myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
			myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void kayit(View view) {
		try {
			new AlertDialog.Builder(this)
					.setTitle("Yeni Kay�t")
					.setMessage("Yeni Kay�t Yapmak �stermisin?")
					.setPositiveButton(android.R.string.yes,
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {

									try {
										kaydediciAc();
										myAudioRecorder
												.setOutputFile(outputFile);
										myAudioRecorder.prepare();
										myAudioRecorder.start();
										butonKontrol(false, false, true, true);
										showMesaj("�iirim Hayat Buluyor", 1000);

									} catch (IllegalStateException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

								}
							})
					.setNegativeButton(android.R.string.no,
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {

									butonKontrol(true, true, false, true);
									dialog.cancel();
								}
							}).setIcon(android.R.drawable.ic_dialog_alert)
					.show();

		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}

	public void kaydet(View view) {
		try {
			myAudioRecorder.stop();
			myAudioRecorder.release();
			myAudioRecorder = null;
		} catch (Exception e) {
			// TODO: handle exception
		}
		butonKontrol(true, true, false, true);
		showMesaj("�iirime Hayat Verdin", 1000);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_taranci, menu);
		return true;
	}

	public void oynat(View view) throws IllegalArgumentException,
			SecurityException, IllegalStateException, IOException {
		try {
			File myfile = new File(outputFile);
			if (myfile.exists()) {
				oynatKontrol += 1;
				if (oynatKontrol % 2 == 0) {

					media = new MediaPlayer();
					media.setDataSource(outputFile);
					media.prepare();
					media.start();
					media.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
						@Override
						public void onCompletion(MediaPlayer media) {
							oynatKontrol += 1;
							butonKontrol(true, true, false, true);
							showMesaj("Canland�rma ��in Tekrar Bas�n�z", 1000);

							media.stop();
							media.release();
						}
					});

					butonKontrol(true, false, false, false);
					showMesaj("�iirim Seninle Ya��yor", 1000);

				} else if (oynatKontrol % 2 != 0) {
					media.stop();
					butonKontrol(true, true, false, true);
					showMesaj("Canland�rma ��in Tekrar Bas�n�z", 1000);

				}
			} else {
				showMesaj("mp3 dosyas� bulunmad�!", 1000);
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void showMesaj(String Mesaj, final long Sure) {
		final Toast a = Toast.makeText(getApplicationContext(), Mesaj,
				Toast.LENGTH_SHORT);
		a.show();

		Thread t = new Thread() {
			@Override
			public void run() {
				try {
					sleep(Sure);
					a.cancel();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
				}
			}
		};
		t.start();
	}

}
