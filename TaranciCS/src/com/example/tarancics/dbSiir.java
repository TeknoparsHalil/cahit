package com.example.tarancics;

public class dbSiir {

	int _id;
	String _baslik;
	String _siir;

	public dbSiir() {

	}

	public dbSiir(int id, String baslik, String siir) {
		this._id = id;
		this._baslik = baslik;
		this._siir = siir;
	}

	public int getID() {
		return this._id;
	}

	public void setID(int id) {
		this._id = id;
	}

	public String getBaslik() {
		return this._baslik;
	}

	public void setBaslik(String baslik) {
		this._baslik = baslik;
	}

	public String getSiir() {
		return this._siir;
	}

	public void setSiir(String siir) {
		this._siir = siir;
	}

}
